# Target enrichment orthology


#### This repository contains scripts for orthology inference from target enrichment data (e.g. Hyb-Seq) and relies mainly on modified scripts from [Phylogenomic dataset construction respository](https://bitbucket.org/yanglab/phylogenomic_dataset_construction/src/master/) plus some new ones.

### Updated August 2023

#### If using the scrips from this repository you must cite

Morales-Briones, D.F., B. Gehrke, H. Chien-Hsun Huang, A. Liston, M. Hong. H.E. Marx, D.C. Tank & Y. Yang. 2022. Analysis of paralogs in target enrichment data pinpoints multiple ancient polyploidy events in *Alchemilla* s.l. (Rosaceae). [ Systematic Biology 71(1):190–207](https://doi.org/10.1093/sysbio/syab032)

Yang, Y. and S.A. Smith. 2014. Orthology inference in non-model organisms using transcriptomes and low-coverage genomes: improving accuracy and matrix occupancy for phylogenomics. Molecular Biology and Evolution. [doi:10.1093/molbev/msu245](https://doi.org/10.1093/molbev/msu245)


### Dependencies needed to run the scripts. 

[TreeShrink](https://github.com/uym2/TreeShrink) It works now with Version 1.3.9 (older versions won't work)

[RAxML](https://github.com/stamatak/standard-RAxML) Version 8.2.11  (newer versions should work)

[Phyx](https://github.com/FePhyFoFum/phyx)

[MAFFT](https://mafft.cbrc.jp/alignment/software/) Version 7.307 (newer versions should work)

[MACSE](https://bioweb.supagro.inra.fr/macse/index.php?menu=releases) Version 2.0.6 (newer versions should work)


## Step 1: Format paralog fasta files

#### **From output of 'paralog_investigator (Hybpiper 1)' or 'paralog_retriever (Hybpiper 2)' of [HybPiper](https://github.com/mossmatters/HybPiper/wiki/Paralogs) or when keeping paralogs from [CAPTUS](https://github.com/edgardomortiz/Captus) (e.g. --max_paralogs -1 in captus_assembly extract)**

### To use the script from [Phylogenomic dataset construction respository](https://bitbucket.org/yanglab/phylogenomic_dataset_construction/src/master/) an "@" symbol needs to be added in the fasta headers to identify paralogs of the same sample.

### For output of Hybiper V1:

The fasta files *.paralogs.fasta after running 'paralog_investigator' has a format from [SPAdes](http://cab.spbu.ru/software/spades/) like

\>Rosa_woodsii  
AGTC...  
\>Lachemilla_pinnata.0 NODE_2_length_1787_cov_230.693567,Fragaria-gene15996_1557_01,4,519,78.47,(+),1,1537  
ACGT.....  
\>Alchemilla_colura.main NODE_3_length_1706_cov_62.896426,Fragaria-gene15996_1557_01,0,517,81.43,(-),1552,1  
ACCC....  
\>Alchemilla_colura.1 NODE_1_length_2101_cov_47.514174,Fragaria-gene15996_1557_01,0,519,79.11,(+),136,1687  
ACCG....  

##### To format the sequences run the loop below in the directory where the fasta files are locaded. Note: This will overwrite the fasta files.

	for i in $(ls *.fasta); do
	sed -i -E 's/(>.+)\.(.+)\s(.+)/\1@paralog_\2/‘ $i
	sed -i '/>/ {/@/! s/$/@unique/}’ $i
	done 
	
The output of this will be

\>Rosa_woodsii@unique  
AGTC...  
\>Lachemilla_pinnata@paralog_0  
ACGT.....  
\>Alchemilla_colura@paralog_main  
ACCC....  
\>Alchemilla_colura@paralog_1  
ACCG....  


### For output of Hybiper V2:

## From paralogs_all directory

Original:

\>Allenrolfea_occidentalis_3077.0 NODE_3_length_897_cov_4.181707,Heteros-TH038267.E2,0,245,90.61,(-1),103,838  
AGTC...  
\>Anabasis_annua_1837.main NODE_1_length_1613_cov_216.759115,EosaxSFB-TH038267.E2,0,252,87.70,(1),419,1175  
ACGT.....
\>Anabasis_aphylla_2411 single_hit  
ACCC....  
\>Salsola_gymnomaschala_0355 multi_hit_stitched_contig_comprising_3_hits  
ACCG....  


**If other additional sequences are added to the fasta files (e.g. from reference genomes) make sure that those also have the "@" form (e.g., I added reference sequences of *Fragaria vesca* as Fragaria_vesca@genome)**


##### To format the sequences run the loop below in the directory where the fasta files are locaded. Note: This will overwrite the fasta files.


	for i in $(ls *.fasta); do
	sed -E -i 's/(>.+)\.(.+)\s(.+)/\1@paralog_\2/' $i
	sed -E -i 's/(>.+)\s(single_hit)/\1@paralog_sh/' $i
	sed -E -i 's/(>.+)\s(multi_hit_stitched_contig_comprising_)([0-9]+)(_hits)/\1@paralog_mh\3/' $i
	done


Output:

\>Allenrolfea_occidentalis_3077@paralog_0  
AGTC...  
\>Allenrolfea_occidentalis_3077@paralog_main  
ACGT.....  
\>Anabasis_aphylla_2411@paralog_sh  
ACCC....  
\>Salsola_gymnomaschala_0355@paralog_mh3  
ACCC....  


## From paralogs_no_chimeras directory

Original:

\>Allenrolfea_occidentalis_3077.0 NODE_3_length_897_cov_4.181707,Heteros-TH038267.E2,0,245,90.61,(-1),103,838  
AGTC...  
\>Anabasis_annua_1837.main NODE_1_length_1613_cov_216.759115,EosaxSFB-TH038267.E2,0,252,87.70,(1),419,1175  
ACGT.....
\>Anabasis_aphylla_2411  Flag no_stitched_contig used. Single longest hit NODE_1_length_1267_cov_815.775630,Suaeda_salsa-TH005657.E3,0,224,99.55,(1),315,987  
ACCC....  



	for i in $(ls *.fasta); do
	sed -E -i 's/(>.+)\s(Flag no_stitched_contig used. Single longest hit.+)/\1@sh/' $i
	sed -E -i 's/(>.+)\.(.+)\s(.+)/\1@pg_\2/' $i
	done


Output:

\>Allenrolfea_occidentalis_3077@pg_0  
AGTC...  
\>Allenrolfea_occidentalis_3077@pg_main  
ACGT.....  
\>Anabasis_aphylla_2411@sh  
ACCC....  



##### For details of SPAdes contigs see [HybPiper's paralogs description](https://github.com/mossmatters/HybPiper/wiki/Paralogs)


### For output of CAPTUS:


Original:

\>Cypripedium_calceolus_01__00 [query=Cypripedium_formosanum_trs__01-LOC110103004] [hit=00] [wscore=0.660] [cover=79.38] [ident=92.75] [score=0.679] [length=1779] [frameshifts=852]  
AGTC...  
\>Cypripedium_farreri_06__01 [query=Cypripedium_formosanum_trs__03-LOC110103004] [hit=01] [wscore=0.257] [cover=56.87] [ident=87.56] [score=0.427] [length=1227] [frameshifts=446,447]  
AGTC...  
\>Cypripedium_formosanum_09 [query=Cypripedium_fargesii_trs-LOC110103004] [hit=00] [wscore=0.637] [cover=79.25] [ident=90.29] [score=0.639] [length=1023]  
ACCC....  


##### To format the sequences run the loop below in the directory where the fasta files are locaded. Note: This will overwrite the fasta files.


	for i in $(ls *.fna); do
	sed -i -E 's/(>.+)(__)([[:digit:]]+)( .+)/\1@paralog_\3/' $i
	sed -i -E 's/(>.+)( \[query=.+)/\1@paralog_unique/' $i
	done


Output

\>Cypripedium_calceolus_01@paralog_00  
AGTC...  
\>Cypripedium_farreri_06@paralog_00  
AGTC...  
\>Cypripedium_formosanum_09@unique  
ACCC....  


## Step 2: Build homolog trees

I used MACSE for DNA alignment. MACSE takes codon structure into account but does not have multithread options, so I wrote individual bash files to run them in parallel.

##### To write bash files. This assumes that you fasta files are in the format **gene1234.E1.fa**

	for filename in $(ls *.fa)
	do
	echo java -jar macse_v2.06.jar -prog alignSequences -seq $filename -out_NT $(cut -d'.' -f1-2 <<<"$filename").NT.aln -out_AA $(cut -d'.' -f1-2 <<<"$filename").AA.aln >> macse_commands.sh
	done

##### To run alignment runs in parallel (e.g. 32 simultaneously runs)

	parallel -j 32 :::: macse_commands.sh

If there are frame shifts in the alignments, MACSE will replace the shifted codons with "!" and will cause problems with RAxML or IQtree. 


##### To replace "!" codon with gaps

	python remove_shifted_codons_from_macse.py <alignment directory> <fasta file extension> <output directory> <nt or aa>
		
	
##### Alternatively, MAFFT can be used for alignment. MAFFT is faster but can only do amino acid or DNA models. It does not have codon models. 

 	python mafft_wrapper.py <fasta files directory> <fasta file extension> <# of threads> <dna or aa>
 	
 	
##### Trim alignments with Phyx

	python pxclsq_wrapper.py <alignment directory > <mininum column occupancy> <dna or aa>
	
The output will files with extension .aln-cln


##### Infer ML trees with RAxML. This will infer trees with GTRGAMMA and 100 bootstrap replicates (different model and # of bs replicates can be modify in the script).  

	python raxml_bs_wrapper.py <aln-cln files directory> <# of threads> <dna or aa>
	
	
##### If no bs replicates are needed use. 
	
	python raxml_wrapper.py <aln-cln files directory> <# of threads> <dna or aa>
	
	
##### If using IQ-TREE you can write bash files to run them in parallel. This assumes that you fasta files are in the format **gene1234.E11.NT.fs.aln-cln**


	for i in $(ls *.aln-cln)
	do
	echo iqtree2 -m MFP --wbtl  -s $i -T 4 --seqtype DNA --prefix $(cut -d'_' -f1-4 <<<"$i").iqtree > $(cut -d'_' -f1-4 <<<"$i").iqtree2.sh
	echo rm  $(cut -d'_' -f1-4 <<<"$i").iqtree.bionj >> $(cut -d'_' -f1-4 <<<"$i").iqtree2.sh
	echo rm  $(cut -d'_' -f1-4 <<<"$i").iqtree.ckp.gz >> $(cut -d'_' -f1-4 <<<"$i").iqtree2.sh
	echo rm  $(cut -d'_' -f1-4 <<<"$i").iqtree.contree >> $(cut -d'_' -f1-4 <<<"$i").iqtree2.sh
	echo rm  $(cut -d'_' -f1-4 <<<"$i").iqtree.mldist >> $(cut -d'_' -f1-4 <<<"$i").iqtree2.sh
	echo rm  $(cut -d'_' -f1-4 <<<"$i").iqtree.model.gz >> $(cut -d'_' -f1-4 <<<"$i").iqtree2.sh
	echo rm  $(cut -d'_' -f1-4 <<<"$i").iqtree.uniqueseq.phy >> $(cut -d'_' -f1-4 <<<"$i").iqtree2.sh
	done


##### To run alignment runs in parallel (e.g. 32 simultaneously runs)

	parallel -j 32 bash ::: *.sh


##### IQ-TREE will replace it with an underscore "_", so you need to add it back

	for i in $(ls *.treefile)
	do
	perl -p -i -e "s/_paralog/\@paralog/g" $i
	done
	
	
##### Mask both mono- and paraphyletic tips that belong to the same taxon. Keep the tip that has the most un-ambiguous characters in the trimmed alignment. If you choose "n" for the parameter "mask_paraphyletic", it will only mask monophyletic tips. 

	python mask_tips_by_taxonID_transcripts.py <tree files directory> <tree files ending> <aln-cln files directory> mask_paraphyletic <y or n> <output directory>
	

##### Alternatively, to remove only monophyletic tips and keep the sequence with the shortest terminal branch length.

	python mask_tips_by_taxonID_genomes.py <tre files directory>
	

##### Trim spurious tips with [TreeShrink](https://github.com/uym2/TreeShrink)

	python tree_shrink_1.3.9_wrapper.py <input directory> <tree file extension> <quantile> <output directory> <exceptions (optional)>

It outputs the tips that were trimmed in the file .txt and the trimmed trees in the files .ts. You would need to test different quantiles to see which one fit better your data. Make sure you open the tree file to check whether TreeShrink removes your outgroups. 


##### These are you final homologs trees. If you want to make a final tree inference (recommended), write fasta files from trimmed trees. 

	python write_homolog_fasta_from_multiple_aln.py <original "@" formated fasta files directory> <trimmed trees directory> <fasta file extension> <tree file extension> <output directory>


## Step 3: Paralogy pruning to infer orthologs. Use one of the following


#### For details of orthology inference refer to Yang and Smith (2014) [doi:10.1093/molbev/msu245](https://doi.org/10.1093/molbev/msu245)



##### 1to1: filter homologs that contains only one sequence per species. No cutting is carried out.
	
	python filter_1to1_orthologs.py <final homologs directory> <tree file extension> <minimal number of taxa> <output directory>


##### MO: prune by using homologs with monophyletic, non-repeating outgroups, reroot and cut paralog from root to tip. If no outgroup, only use those that do not have duplicated taxa. Set OUTPUT_1to1_ORTHOLOGS to False if wish only to ouput orthologs that are not 1to1. The 1to1 orthologs here are also required to have a monophyletic outgroup.

	python prune_paralogs_MO_1to1_MO.py <final homologs directory> <tree file extension> <minimal number of taxa> <output directory> <ingroup and outgroup taxonIDs table>


##### RT: prune by extracting ingroup clades and then cut paralogs from root to tip. If no outgroup, only use those that do not have duplicated taxa. Compile a list of ingroup and outgroup taxonID, with each line begin with either "IN" or "OUT", followed by a tab, and then the taxonID.

	python prune_paralogs_RT.py <final homologs directory> <tree file extension> <output directory>  <minimal number of taxa> <ingroup and outgroup taxonIDs table>


## Step 4: Visualize matrix occupancy stats, write final fasta files and build supermatrix (optional)

	python ortholog_occupancy_stats.py <orthologs directory>


##### Read in and rank number of taxa per ortholog from highest to lowest. Plot in R the ranked number of taxa per ortholog

	a <- as.numeric(read.table("ortho_stats")[,1])
	a <- sort(a, decreasing=TRUE)
	pdf(file="taxon_occupancy.pdf")
	plot(a, type="l", lwd=3, ylab="Number of Taxa in Each Ortholog")
	dev.off()


##### Check taxon_stats to see if any taxa have unusally low number of genes in the orthologs and decide the minimum number of taxa for the supermatrix. 

##### Write new and final fasta files from ortholog trees

	python write_ortholog_fasta_from_multiple_aln.py <original "@" formated fasta files directory> <orthologs directory> <fasta file extension> <orthologs tree file extension> <output directory>
	
**If using RT orthologs use:***

	python write_ortholog_fasta_from_multiple_aln_RT_.py <original "@" formated fasta files directory> <orthologs directory> <fasta file extension> <orthologs tree file extension> <output directory>

	
##### With those you can align, clean, infer trees from final orthologs, and infer species trees using your preferred tools

 
##### If a supermatrix of clean alignment is needed. Once you have clean alignments, choose the minimal cleaned alignment length and minimal number of taxa of each ortholog to include in the supermatrix

	python concatenate_matrices_phyx.py <aln-cln files directory> <minimum number of sites> <minimum number of taxa> <output directory>

This will output a list of cleaned orthology alignments that passed the filter, a summary of taxon matrix occupancies to check whether any taxon is under represented, and a concatenated matrix in fasta, phylip, and nexus format as well as a partition file for RAxML or IQtree.




## Plotting PhyParts pie charts

[PhyParts](https://bitbucket.org/blackrim/phyparts/src/master/) is a great tool to explore gene tree conflict and Matt Johnson has a great pie chart visualization script [here](https://github.com/mossmatters/MJPythonNotebooks/blob/master/PhyParts_PieCharts.ipynb)

Still, this visualization assumes a fix number informative gene trees for all nodes. In other words, assumes no missing taxa in the ortholog gene trees. While this can be the case of genomic or filtered transcriptomic datasets or MO orthologs, this is not necessary the case for RT othologs, or when there are missing taxa in the gene trees like the case of target enrichment datasets or patchy transcriptomic/genomic datasets. The assumption of equal number of informing gene trees also does not apply with running PhyParts with homologs trees, where duplications and missing taxa will most likely be present.

If we use a fixed number of genes for orthologs with missing taxa, the 'grey' part of the pie charts will be the sum of missing and uninformative nodes. If there is a lot of missing data, this can be misleading and give the false impression of a lot of uninformative nodes (below the support treshold used when running PhyParts). In the case of homologs, the proportion in all pie charts will be incorrect because of gene duplication (each node might have a different number of gene trees) in addition to the problem with the missing and uninformative nodes.

To fix this we can plot the slices in pie charts proportional to the number of informing nodes. If you ran a full concordance analyses (-a 1) with support cutoff option (-s), PhyParts only report the total of concordant and discordant nodes that passed the support threshold, but it is missing the number of nodes that did not pass. To get this number we can run an addional quick concordant analyses (-a 0) without the support cutoff option (-s) and combine the information of both analyses.

We can combine the information of both analyses to get files to plot proportional pie charts in R:

	library(phytools)

	read.tree("A_0_No_BS_RT_all_homologs.concon.tre")->No_bs #Tree file output from phyparts of concordance (-a 0) anlysis with out support filtering (-s)
	read.tree("A_1_BS_50_RT_all_homologs.concon.tre")->bs_full_concordance #Tree file output from phyparts of full concordance anlysis (-a 1) with support filtering (e.g. Bootstrap 50; -s 50)
	
	total_no_bs<-No_bs[[1]] # get a tree to add total node numbers
	
	total_no_bs$node.label<-mapply("+",as.numeric(No_bs[[1]]$node.label), as.numeric(No_bs[[2]]$node.label)) #get total number of nodes
	total_no_bs$node.label[is.na(total_no_bs$node.label)] <- "" #remove NA values
	total_no_bs$node.label[total_no_bs$node.label=="0"]<-"" #remove 0 values. to avoid divisions by zero.
	
	
	append(bs_full_concordance, total_no_bs, after=2)-> full_concordance_and_total_nodes #append tree with total number of nodes to tree file output from phyparts of full concordance anlysis
	
	write.tree(full_concordance_and_total_nodes, file = "A_1_BS_50_RT_all_homologs.concon.tre") #write tree. this will replace to orignal file.


Then we can used the the following script to plot pie charts that each is proportional to the total number of informing genes.

	python phypartspiecharts_proportional.py <species_tree> <phyparts_prepend_output_files>
	
The 'species_tree' is the map tree that you used for phyparts and 'phyparts_prepend_output_files' is the prefix of PhyParts outpuf files of the full concordance analyses (-a 1) with the modified *.concon.tre file from R.

When plotting pie charts of analyses that used homologs, you should always use 'proportional' script.  


Other option for ortholog gene trees is to plot the missing and uninformative separately as it own slice of the pie chart. To do this you can use the following script.

 	python phypartspiecharts_missing_uninformative.py <species_tree> <phyparts_prepend_output_files> <number of genes>
 	
Those pie charts will look similar if using the original script, but the 'grey' part is now divided into uninformative and missing. The 'number of genes' is the number of input gene trees for the PhyParts .  




#### Pie charts examples

Pie charts made with the [original script](https://github.com/mossmatters/MJPythonNotebooks/blob/master/phypartspiecharts.py) using a fix number of gene trees look like this. Because we have have missing taxa in the gene trees the grey slice represent the uninformative and missing together. This gives the impression of a lot uninformative genes trees for those nodes.  


![original](images/1_Original.png)   


Pie charts made with 'phypartspiecharts_missing_uninformative.py.' In this case the uninformative are 'dark grey' and missing 'light grey'. The total number of gene trees is the same as in the original script.  


![missing](images/2_Missing_and_uninfomtaive.png) 

  
Pie charts made with the 'phypartspiecharts_proportional.py.' In this case each pie charts is proportional to the total number of informing genes for that node and do not take in account the missing ones. The grey part represent only the uninformative gene trees. Always use this option when plotting Phyparts analyses of homolog gene trees.

![proportional](images/3_Proportional.png)
