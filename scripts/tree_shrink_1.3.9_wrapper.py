"""
Detects and trim abnormally long branches using TreeShrink v1.3.9

TreeShrink must be installed and on path
Phyx (pxrr) must be installed and on path

"""


import sys, os, shutil, glob, re
from Bio import Phylo
import pandas as pd


def trim_exceptions(inDIR,tree_file_ending,q,outDIR,exceptions):

	if os.path.isabs(inDIR) == False: inDIR = os.path.abspath(inDIR)
	if inDIR[-1] != "/": inDIR += "/"
	if os.path.isabs(outDIR) == False: outDIR = os.path.abspath(outDIR)
	if outDIR[-1] != "/": outDIR += "/"
	if outDIR == ".": outDIR = os.getcwd()
	
	
	filecount = 0

	with open(exceptions) as f:
	    taxa_to_not_remove = f.read().splitlines()
	    #print taxa_to_not_remove

	

	for i in os.listdir(inDIR):
		if i.endswith(tree_file_ending):
			original_tre = Phylo.read(inDIR+i, "newick")
			original_tre_tips = []
			for leaf in original_tre.get_terminals(): original_tre_tips.append(leaf.name)
			ori_tip_db = pd.DataFrame(original_tre_tips)
			#print ori_tip_db
			
	    
			newlist = []
			for taxa in taxa_to_not_remove:
				r = re.compile(str(taxa)+".*")
				newlist.append(filter(r.match, original_tre_tips))
				
			flatlist = []
			for sublist in newlist:
				for item in sublist:
					flatlist.append(item)
			
			print(flatlist)
			
			if len(flatlist) >= 1:
			
				exception_file = outDIR+i+".exceptions.ext"
				with open(exception_file, "w") as file:
					for item in flatlist:
   				 		file.write("%s\n" % item)
						
	
				os.path.splitext(outDIR+i)[0]
				#print os.path.splitext(outDIR+i)[0]
				filecount += 1
				cmd= ["run_treeshrink.py","-t", inDIR+i ,"-c","-m per-gene", "-q "+str(q), "-o",outDIR+i+".ts_dir", "-O "+os.path.splitext(outDIR+i)[0], "-x",exception_file]
				print (" ".join(cmd))
				os.system(" ".join(cmd))
				
			
			else: 

				os.path.splitext(outDIR+i)[0]
				#print os.path.splitext(outDIR+i)[0]
				filecount += 1
				cmd= ["run_treeshrink.py","-t", inDIR+i ,"-c","-m per-gene", "-q "+str(q), "-o",outDIR+i+".ts_dir", "-O "+os.path.splitext(outDIR+i)[0]]
				print (" ".join(cmd))
				os.system(" ".join(cmd))
								

	#moves output files to DIR and delete treeshrink individual folders
	for j in os.listdir(outDIR):
		if j.endswith(".ts_dir"):
			source = outDIR+j
			dest = outDIR
			files = os.listdir(source)
			for f in files:
				shutil.move(source+"/"+f, dest)
			shutil.rmtree(outDIR+j)
	
	
	#removes single quotes from tip labels from treeshrink output trees
	for k in os.listdir(outDIR):
		if k.endswith(".mm"):
			with open(outDIR+k, 'r+') as f:
				content = f.read()
				f.seek(0)
				f.truncate()
				f.write(content.replace("'", ""))
			f.close()
			
			
	#removes remove root edge (if present). This avoid issues with ortholog prunning scripts.les
	for n in os.listdir(outDIR):
		if n.endswith(".mm"):
			cmd= ["pxcltr","-r","-t", outDIR+n,"-o",outDIR+n+".tr"]
			#print (" ".join(cmd))
			os.system(" ".join(cmd))			
			
				
	#unroot treeshrink ouput trees
	for l in os.listdir(outDIR):
		if l.endswith(".tr"):
			cmd= ["pxrr","-u","-t", outDIR+l,"-o",outDIR+l+".ts"]
			#print (" ".join(cmd))
			os.system(" ".join(cmd))			
			
	
	#delete tr files
	for m in os.listdir(outDIR):
			if m.endswith(".tr"):
    				os.remove(outDIR+m)
	
	#delete ts files
	for m in os.listdir(outDIR):
			if m.endswith(".mm"):
    				os.remove(outDIR+m)
    				
	#delete extension files
	for o in os.listdir(outDIR):
			if o.endswith(".ext"):
    				os.remove(outDIR+o)

	#delete summary files
	for p in os.listdir(outDIR):
			if p.endswith("treefile_summary.txt"):
    				os.remove(outDIR+p)    				


    				
def trim(inDIR,tree_file_ending,q,outDIR):

	if os.path.isabs(inDIR) == False: inDIR = os.path.abspath(inDIR)
	if inDIR[-1] != "/": inDIR += "/"
	if os.path.isabs(outDIR) == False: outDIR = os.path.abspath(outDIR)
	if outDIR[-1] != "/": outDIR += "/"
	if outDIR == ".": outDIR = os.getcwd()
	
	
	filecount = 0


	for i in os.listdir(inDIR):
		if i.endswith(tree_file_ending):

			os.path.splitext(outDIR+i)[0]
			#print os.path.splitext(outDIR+i)[0]
			filecount += 1
			cmd= ["run_treeshrink.py","-t", inDIR+i ,"-c","-m per-gene", "-q "+str(q), "-o",outDIR+i+".ts_dir", "-O "+os.path.splitext(outDIR+i)[0]]
			print (" ".join(cmd))
			os.system(" ".join(cmd))
								

	#moves output files to DIR and delete treeshrink individual folders
	for j in os.listdir(outDIR):
		if j.endswith(".ts_dir"):
			source = outDIR+j
			dest = outDIR
			files = os.listdir(source)
			for f in files:
				shutil.move(source+"/"+f, dest)
			shutil.rmtree(outDIR+j)
	
	
	#removes single quotes from tip labels from treeshrink output trees
	for k in os.listdir(outDIR):
		if k.endswith(".mm"):
			with open(outDIR+k, 'r+') as f:
				content = f.read()
				f.seek(0)
				f.truncate()
				f.write(content.replace("'", ""))
			f.close()
			
			
	#removes remove root edge (if present). This avoid issues with ortholog prunning scripts.les
	for n in os.listdir(outDIR):
		if n.endswith(".mm"):
			cmd= ["pxcltr","-r","-t", outDIR+n,"-o",outDIR+n+".tr"]
			#print (" ".join(cmd))
			os.system(" ".join(cmd))			
			
				
	#unroot treeshrink ouput trees
	for l in os.listdir(outDIR):
		if l.endswith(".tr"):
			cmd= ["pxrr","-u","-t", outDIR+l,"-o",outDIR+l+".ts"]
			#print (" ".join(cmd))
			os.system(" ".join(cmd))			
			
	
	#delete tr files
	for m in os.listdir(outDIR):
			if m.endswith(".tr"):
    				os.remove(outDIR+m)
	
	#delete ts files
	for m in os.listdir(outDIR):
			if m.endswith(".mm"):
    				os.remove(outDIR+m)
    				

	#delete summary files
	for p in os.listdir(outDIR):
			if p.endswith("treefile_summary.txt"):
    				os.remove(outDIR+p)    				
    				
        
            
	assert filecount > 0, \
		"No file end with "+tree_file_ending+" found in "+inDIR
			
if __name__ == "__main__":

	if len(sys.argv) == 6:
		inDIR,tree_file_ending,q,outDIR,exceptions = sys.argv[1:]
		trim_exceptions(inDIR,tree_file_ending,q,outDIR,exceptions)
	
	elif len(sys.argv) == 5:
		inDIR,tree_file_ending,q,outDIR = sys.argv[1:]
		trim(inDIR,tree_file_ending,q,outDIR)
		
	else:
		print ("Usage:")
		print ("python tree_shrink_wrapper.py inDIR tree_file_ending quantile outDIR exceptions(optional)")
		sys.exit(0)	
	

	
	
