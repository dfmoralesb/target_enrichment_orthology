import os,sys,io
from Bio import Phylo


def short_tip_lables(inMultiTree):

	
	basename = os.path.splitext(inMultiTree)[0]
	cwd = os.getcwd()
	new_mulititree= cwd+"/"+basename+"_short_label"+".tre"
		
	f= open(new_mulititree,"w")
	
	trees = Phylo.parse(inMultiTree, 'newick')
	
	for tree in trees:
		for leaf in tree.get_terminals():
			leaf.name = leaf.name.split("@", 1)[0]
			#print (leaf.name)
		Phylo.write(tree, f, 'newick')
	
	f.close()	
			
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print ("Usage:")
		print ("python short_tip_lables.py inMultiTree")
	else:	
		short_tip_lables(inMultiTree=sys.argv[1])
	sys.exit(0)