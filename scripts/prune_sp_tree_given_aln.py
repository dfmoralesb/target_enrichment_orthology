import os, sys, re, shutil
from Bio import Phylo
import pandas as pd
from Bio import SeqIO


def remove_taxa(inDIR, fasta_ending, outDIR, sptree):
	""""Takes a list of taxa and remove it from trees"""

	if os.path.isabs(inDIR) == False: inDIR = os.path.abspath(inDIR)
	if inDIR[-1] != "/": inDIR += "/"
	
	if os.path.isabs(outDIR) == False: outDIR = os.path.abspath(outDIR)
	if outDIR[-1] != "/": outDIR += "/"
	
	if outDIR == ".": outDIR = os.getcwd()

	filecount = 0
	
	
	sp_tree = Phylo.read(sptree, "newick")
	sp_tree_tips = []
	for leaf in sp_tree.get_terminals(): sp_tree_tips.append(leaf.name)
	print sp_tree_tips
	sp_tree_db = pd.DataFrame(sp_tree_tips)
	
	for i in os.listdir(inDIR):
		if i.endswith(fasta_ending):
			print i
			filecount += 1
			original_fasta = SeqIO.parse(inDIR+i, "fasta")
			original_fasta_names = []
			for record in original_fasta: original_fasta_names.append(record.id)
			fasta_names_db = pd.DataFrame(original_fasta_names)
			#print original_fasta_names
	
			path_fasta, files_fasta = os.path.split(i)
			fasta_name = str(files_fasta)
			base_name_fasta = fasta_name.split( "." )
			#print base_name_fasta
			
			
			tips_concat = pd.concat([sp_tree_db, fasta_names_db])
			tips_concat.columns = ['tip_labels']
			uniques = tips_concat.drop_duplicates(keep=False)
			list = uniques['tip_labels'].values.tolist()
			
			if len(list) == 0:
				shutil.copy2(sptree, (outDIR+base_name_fasta[0]+".sptree.tre"))
			
			else:
				cmd = ["pxrmt","-o", (outDIR+base_name_fasta[0]+"sptree.tre"),"-t", sptree,"-n"]
				print ((" ".join(cmd))+(" ")+(','.join(list)))
				os.system((" ".join(cmd))+(" ")+(','.join(list)))
        
            
	assert filecount > 0, \
		"No file end with "+tree_file_ending+" found in "+inDIR

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python prune_sp_tree_given_aln.py inDIR  fasta_ending  outDIR  sptree"
		sys.exit(0)

	inDIR, fasta_ending, outDIR, sptree = sys.argv[1:]
	remove_taxa(inDIR, fasta_ending, outDIR, sptree)




