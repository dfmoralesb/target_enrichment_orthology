###This script removes sequences that have codon shifts (!) from Macse alginments


import os, sys, re
from Bio import SeqIO
from Bio import AlignIO


def remove_sequences(inDIR,file_ending,outDIR):

	if os.path.isabs(inDIR) == False: inDIR = os.path.abspath(inDIR)
	if inDIR[-1] != "/": inDIR += "/"
	if os.path.isabs(outDIR) == False: outDIR = os.path.abspath(outDIR)
	if outDIR[-1] != "/": outDIR += "/"
	if outDIR == ".": outDIR = os.getcwd()
	
	
	filecount = 0

	for i in os.listdir(inDIR):
		if i.endswith(file_ending):
			print (i)
			filecount += 1
			original_fasta = SeqIO.parse(inDIR+i, "fasta")
			original_fasta_names = []
			for record in original_fasta: original_fasta_names.append(record.id)
			#print (original_fasta_names)

			path_fasta, files_fasta = os.path.split(i)
			fasta_name = str(files_fasta)
			base_name_fasta = fasta_name.split( "." )
			#print (base_name_fasta)

			taxa_to_remove = []
			original_fasta = SeqIO.parse(inDIR+i, "fasta")
			for record in original_fasta:
				if "!" in record.seq:
					taxa_to_remove.append(record.id)
			#print (taxa_to_remove)
			

			new_fasta_file = outDIR+base_name_fasta[0]+"."+base_name_fasta[1]+".no_fs_seq.aln.fa"
			#print new_fasta_file
			file=open(new_fasta_file,'w')
	
			for record in SeqIO.parse(inDIR+i,"fasta"):
				if record.id not in taxa_to_remove:
				    id =record.id
				    seq=str(record.seq)
				    file.write(">" + id + "\n" + seq + "\n")
			file.close()
			

			
	assert filecount > 0, \
		"No file end with "+file_ending+" found in "+inDIR
						

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print ("Usage:")
		print ("python remove_sequences_with_frameshft.py inDIR, file_ending, outDIR")
	else:	
		remove_sequences(inDIR=sys.argv[1],file_ending=sys.argv[2],outDIR=sys.argv[3])

	sys.exit(0)		